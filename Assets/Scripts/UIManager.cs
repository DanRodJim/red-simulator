﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static InputField[] inputsIPv4;
    public static InputField[] inputsIPv6;

    public InputField mask;

    public static List<int> ipv4PC1;
    public static List<int> ipv4PC2;
    public static List<int> ipv4PC3;
    public static List<int> ipv4SW1;
    public static List<int> ipv4SW2;

    /* public static Text router1IP;
    public static Text router2IP;
    public static Text switch1IP;
    public static Text switch2IP;
    public static Text wifiIP; */

    public Dropdown dropdown;

    public Toggle toggleIPv4;
    public Toggle toggleIPv6;

    void Start()
    {
        ipv4PC1 = new List<int>{0,0,0,0};
        ipv4PC2 = new List<int>{0,0,0,0};
        ipv4PC3 = new List<int>{0,0,0,0};
    }

    void Update()
    {
        //Debug.Log(ipv4PC1[0]);
        if(this.gameObject.name == "pc1"){
            inputsIPv4[0].text = ipv4PC1[0].ToString();
            inputsIPv4[1].text = ipv4PC1[1].ToString();
            inputsIPv4[2].text = ipv4PC1[2].ToString();
            inputsIPv4[3].text = ipv4PC1[3].ToString();
        }
        else if(this.gameObject.name == "pc2"){
            inputsIPv4[0].text = ipv4PC2[0].ToString();
            inputsIPv4[1].text = ipv4PC2[1].ToString();
            inputsIPv4[2].text = ipv4PC2[2].ToString();
            inputsIPv4[3].text = ipv4PC2[3].ToString();
        }
        else if(this.gameObject.name == "pc3"){
            inputsIPv4[0].text = ipv4PC3[0].ToString();
            inputsIPv4[1].text = ipv4PC3[1].ToString();
            inputsIPv4[2].text = ipv4PC3[2].ToString();
            inputsIPv4[3].text = ipv4PC3[3].ToString();
        }
        else if(this.gameObject.name == "switch1"){
            inputsIPv4[0].text = ipv4SW1[0].ToString();
            inputsIPv4[1].text = ipv4SW1[1].ToString();
            inputsIPv4[2].text = ipv4SW1[2].ToString();
            inputsIPv4[3].text = ipv4SW1[3].ToString();
        }
        else if(this.gameObject.name == "switch2"){
            inputsIPv4[0].text = ipv4SW2[0].ToString();
            inputsIPv4[1].text = ipv4SW2[1].ToString();
            inputsIPv4[2].text = ipv4SW2[2].ToString();
            inputsIPv4[3].text = ipv4SW2[3].ToString();
        }
        //Debug.Log(ipv4PC1[0]);
    }

    public void Dropdown_IndexChanged(int index){
        if(index == 0){
            toggleIPv4.interactable = false;
            toggleIPv6.interactable = false;
            foreach(InputField inF in inputsIPv4){
                inF.interactable = false;
            }
            foreach(InputField inF in inputsIPv6){
                inF.interactable = false;
            }
        }
        else if(index == 1){
            toggleIPv4.interactable = true;
            toggleIPv6.interactable = true;
        }
        else{
            toggleIPv4.interactable = true;
            toggleIPv6.interactable = true;
        }
        toggleIPv4.isOn = false;
        toggleIPv6.isOn = false;
    }
    
    public void Toggle_Changed(bool i){
        if(this.gameObject.name == "ActIPv4"){
            foreach(InputField inF in inputsIPv4){
                inF.interactable = i;
            }
        }
        else{
            foreach(InputField inF in inputsIPv6){
                inF.interactable = i;
            }
        }
    }

    public void subnetear(){
        mask.interactable = !mask.interactable;
    }

    public void changeScene(){
        SceneManager.LoadScene(1);
    }

    public void changeSceneMain(){
        SceneManager.LoadScene(0);
    }

    public void pcIPv4(){
        if(this.gameObject.name == "pc1"){
            ipv4PC1[0] = int.Parse(inputsIPv4[0].text);
            ipv4PC1[1] = int.Parse(inputsIPv4[1].text);
            ipv4PC1[2] = int.Parse(inputsIPv4[2].text);
            ipv4PC1[3] = int.Parse(inputsIPv4[3].text);
        }
        else if(this.gameObject.name == "pc2"){
            ipv4PC2[0] = int.Parse(inputsIPv4[0].text);
            ipv4PC2[1] = int.Parse(inputsIPv4[1].text);
            ipv4PC2[2] = int.Parse(inputsIPv4[2].text);
            ipv4PC2[3] = int.Parse(inputsIPv4[3].text);
        }
        else if(this.gameObject.name == "pc3"){
            ipv4PC3[0] = int.Parse(inputsIPv4[0].text);
            ipv4PC3[1] = int.Parse(inputsIPv4[1].text);
            ipv4PC3[2] = int.Parse(inputsIPv4[2].text);
            ipv4PC3[3] = int.Parse(inputsIPv4[3].text);
        }
        else if(this.gameObject.name == "switch1"){
            ipv4SW1[0] = int.Parse(inputsIPv4[0].text);
            ipv4SW1[1] = int.Parse(inputsIPv4[1].text);
            ipv4SW1[2] = int.Parse(inputsIPv4[2].text);
            ipv4SW1[3] = int.Parse(inputsIPv4[3].text);
        }
        else if(this.gameObject.name == "switch2"){
            ipv4SW2[0] = int.Parse(inputsIPv4[0].text);
            ipv4SW2[1] = int.Parse(inputsIPv4[1].text);
            ipv4SW2[2] = int.Parse(inputsIPv4[2].text);
            ipv4SW2[3] = int.Parse(inputsIPv4[3].text);
        }
    }

    public static void setIP(Text ipv4, string target){

        string ip = ipv4.text;        
        string[] textoIP = ip.Split('.', '/');

        Debug.Log(textoIP[0]+" "+textoIP[1]+" "+textoIP[2]+" "+textoIP[3]);
        if(target == "pc1"){
            ipv4PC1[0] = int.Parse(textoIP[0]);
            ipv4PC1[1] = int.Parse(textoIP[1]);
            ipv4PC1[2] = int.Parse(textoIP[2]);
            ipv4PC1[3] = int.Parse(textoIP[3])+1;
        }
        else if(target == "pc2"){
            ipv4PC2[0] = int.Parse(textoIP[0]);
            ipv4PC2[1] = int.Parse(textoIP[1]);
            ipv4PC2[2] = int.Parse(textoIP[2]);
            ipv4PC2[3] = int.Parse(textoIP[3])+1;
        }
        else if(target == "pc3"){
            ipv4PC3[0] = int.Parse(textoIP[0]);
            ipv4PC3[1] = int.Parse(textoIP[1]);
            ipv4PC3[2] = int.Parse(textoIP[2]);
            ipv4PC3[3] = int.Parse(textoIP[3])+1;
        }
        else if(target == "switch1"){
            ipv4SW1[0] = int.Parse(textoIP[0]);
            ipv4SW1[1] = int.Parse(textoIP[1]);
            ipv4SW1[2] = int.Parse(textoIP[2]);
            ipv4SW1[3] = int.Parse(textoIP[3])+1;
        }
        else if(target == "switch2"){
            ipv4SW2[0] = int.Parse(textoIP[0]);
            ipv4SW2[1] = int.Parse(textoIP[1]);
            ipv4SW2[2] = int.Parse(textoIP[2]);
            ipv4SW2[3] = int.Parse(textoIP[3])+1;
        }
    }
}
