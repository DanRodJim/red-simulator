﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceStats : MonoBehaviour
{
    private bool pc1connected;
    private bool pc2connected;
    private bool pc3connected;
    private bool router1connected;
    private bool router2connected;
    private bool switch1connected;
    private bool switch2connected;
    private bool wifi1connected;
    private bool wifi2connected;

    void Start()
    {
        pc1connected = false;
        pc2connected = false;
        pc3connected = false;
        router1connected = false;
        router2connected = false;
        switch1connected = false;
        switch2connected = false;
        wifi1connected = false;
        wifi2connected = false;
    }

    void setConnection(int n){
        if(n == 0){
            pc1connected = true;
        }
        else if(n == 1){
            pc2connected = true;
        }
        else if(n == 2){
            pc3connected = true;
        }
        else if(n == 3){
            router1connected = true;
        }
        else if(n == 4){
            router2connected = true;
        }
        else if(n == 5){
            switch1connected = true;
        }
        else if(n == 6){
            switch2connected = true;
        }
        else if(n == 7){
            wifi1connected = true;
        }
        else if(n == 8){
            wifi2connected = true;
        }
    }
    
}
