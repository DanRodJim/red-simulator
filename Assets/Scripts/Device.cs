﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Device
{
    public bool pc1connected;
    public bool pc2connected;
    public bool pc3connected;
    public bool router1connected;
    public bool router2connected;
    public bool switch1connected;
    public bool switch2connected;
    public bool wifi1connected;
    public bool wifi2connected;

    public Device(){
        pc1connected = false;
        pc2connected = false;
        pc3connected = false;
        router1connected = false;
        router2connected = false;
        switch1connected = false;
        switch2connected = false;
        wifi1connected = false;
        wifi2connected = false;
    }

    void setConnection(int n){
        if(n == 0){
            pc1connected = true;
        }
        else if(n == 1){
            pc2connected = true;
        }
        else if(n == 2){
            pc3connected = true;
        }
        else if(n == 3){
            router1connected = true;
        }
        else if(n == 4){
            router2connected = true;
        }
        else if(n == 5){
            switch1connected = true;
        }
        else if(n == 6){
            switch2connected = true;
        }
        else if(n == 7){
            wifi1connected = true;
        }
        else if(n == 8){
            wifi2connected = true;
        }
    }
}