﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchDevice : MonoBehaviour
{
    public GameObject menu;
    public GameObject menuRouter;
    string nameObj;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began){
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit Hit;

            if(Physics.Raycast(ray, out Hit)){
                nameObj = Hit.transform.name;
                if(nameObj == "pc1" || nameObj == "pc2" || nameObj == "pc3"){
                    menu.SetActive(true);
                }
                else if(nameObj == "router1" || nameObj == "router2"){
                    menuRouter.SetActive(true);
                }
            }
        }
    }
}
